# How to Visualize Markdown File

**15/03/1994** 

​	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. 

## First Part

​	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, nostrud ullamco laboris nisi ut aliquip ex ea  commodo consequat. 

### Section

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. 

> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. 

Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. 

#### sub section

​	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. 

| First Column | Second Column | Third Column |
| ------------ | ------------- | ------------ |
| 1.233        | 32.323        | 2442.121     |
| 825          | 3523          | 5455344      |
| 866          | 342           | 424          |



#### other sub-section

**Lorem ipsum dolor sit amet:**

1. Consectetur adipiscing elit
2. Sed do eiusmod
3. Tempor incididunt ut labore
4. Dolore magna aliqua. 

**Lorem ipsum dolor sit amet:**

- Consectetur adipiscing elit
- Sed do eiusmod
	- Tempor incididunt ut labore
	  - Dolore magna aliqua. 

**Lorem ipsum dolor sit amet:**

- [ ] Consectetur adipiscing elit
- [x] Sed do eiusmod
- [ ] Tempor incididunt ut labore
- [ ] Dolore magna aliqua. 

##### Way too deep title

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. 

![vertical image](https://www.maxpixels.net/static/photo/1x/Internet-Technology-Mobile-Communication-Phone-1412636.jpg)  


### Next Section

Lorem ipsum **dolor** sit amet, *consectetur adipiscing elit*, sed do ~~eiusmod tempor~~ incididunt ut labore et dolore magna aliqua. Ut enim ad `minim veniam`, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. [Lorem ipsum](Lorem ipsum.md) dolor sit amet, consectetur adipiscing elit, sed do eiusmod  ==tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi== ut aliquip ex ea  commodo consequat. 



##  Seccond Part

**Lorem ipsum** dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. 

![horizontal image](https://www.maxpixels.net/static/photo/1x/Blur-Detail-Document-Close-up-Designer-Design-1867746.jpg)

# Appendix

## For Programmers

<!-- add comments -->

You can add sample of code

``` java
/**
* This is a sample of code
*/
public class Main {
    public static void main(String[] args) {
        System.out.println("Code sample");
    }
}
```
### Class Diagrams
```mermaid
classDiagram
      Animal <|-- Duck
      Animal <|-- Fish
      Animal <|-- Zebra
      Animal : +int age
      Animal : +String gender
      Animal: +isMammal()
      Animal: +mate()
      class Duck{
          +String beakColor
          +swim()
          +quack()
      }
      class Fish{
          -int sizeInFeet
          -canEat()
      }
      class Zebra{
          +bool is_wild
          +run()
      }
```

## For Scientifics

You can add equations
$$
\frac{\part\phi}{\part t}
+
\nabla ⋅(\phi\mathbf{V}) 
= S 
\tag{$\star$}
$$

## Charts

### Pie Chart

```mermaid
pie
    title Pie Chart
    "Dogs" : 386
    "Cats" : 85
    "Rats" : 150 
```

### State Diagrams

```mermaid
stateDiagram
    [*] --> Still
    Still --> [*]
	Still --> Moving
    
    Moving --> Still
    Moving --> Crash 
    
    Crash --> [*]
```

### Gantt Chart

```mermaid
%% Example with selection of syntaxes
        gantt
        dateFormat  YYYY-MM-DD
        title Adding GANTT diagram functionality to mermaid

        section A section
        Completed task            :done,    des1, 2014-01-06,2014-01-08
        Active task               :active,  des2, 2014-01-09, 3d
        Future task               :         des3, after des2, 5d
        Future task2               :         des4, after des3, 5d

        section Critical tasks
        Completed task in the critical line :crit, done, 2014-01-06,24h
        Implement parser and jison          :crit, done, after des1, 2d
        Create tests for parser             :crit, active, 3d
        Future task in critical line        :crit, 5d
        Create tests for renderer           :2d
        Add to mermaid                      :1d

        section Documentation
        Describe gantt syntax               :active, a1, after des1, 3d
        Add gantt diagram to demo page      :after a1  , 20h
        Add another diagram to demo page    :doc1, after a1  , 48h

        section Last section
        Describe gantt syntax               :after doc1, 3d
        Add gantt diagram to demo page      : 20h
        Add another diagram to demo page    : 48h
```
